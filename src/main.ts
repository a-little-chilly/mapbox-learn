import { createApp } from 'vue'
import './style.css'
import '@/assets/style/index.scss'
import App from './App.vue'
import Antd from 'ant-design-vue'
import 'ant-design-vue/dist/reset.css'
import 'mapbox-gl/dist/mapbox-gl.css'

const app = createApp(App)

app.use(Antd).mount('#app')
